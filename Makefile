CRYSTAL_BIN ?= $(shell which crystal)

all: clean compile

compile:
	$(CRYSTAL_BIN) deps
	$(CRYSTAL_BIN) build --release -o build/spoty src/spoty.cr $(CRFLAGS)

clean:
	rm -f ./build/*