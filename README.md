# Spoty - Spotify MPRIS D-Bus Interface Commander

*Very* simple tool to command spotify playback via its D-BUS MPRIS interface.
Useful for XFCE4 systems in which the media keys do not seem to be mapped by default.

## Usage

With spotify open, just command spotify to do something...

`spoty (play|pause|stop|next|prev)`

That's it ;)

---

Coded by [Estanislau Trepat](http://etrepat.com). I'm also
[@etrepat](http://twitter.com/etrepat) on twitter.