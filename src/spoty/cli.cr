module Spoty
  module CLI
    extend self

    def run(args)
      command = parse_command(args)

      if command.nil?
        print_usage
        return 1
      end

      if command == :unknown
        puts "spoty: Unknown command: '#{command.to_s}"
        return 1
      end

      DBUS.send(command)
      return 0
    end

    private def print_usage
      puts "Spoty v#{VERSION} - Spotify MPRIS D-Bus Interface Commander"
      puts "Usage: 'spoty (play|pause|stop|next|prev)'"
    end

    private def parse_command(args)
      return nil if args.empty?

      return :none unless args[0]

      input = args[0]

      command = input.chomp.strip.downcase

      case command
      when "play", "pause"
        :play_pause
      when "stop"
        :stop
      when "next"
        :next
      when "prev"
        :previous
      else
        :unknown
      end
    end
  end
end