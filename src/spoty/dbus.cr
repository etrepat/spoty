require "process"

module Spoty
  module DBUS
    extend self

    DBUS_CMD = "/usr/bin/dbus-send"

    DBUS_ARGS = ["--print-reply", "--dest=org.mpris.MediaPlayer2.spotify",   "/org/mpris/MediaPlayer2"]

    def send(command)
      args = DBUS_ARGS.clone.concat([to_action(command)])
      Process.exec(DBUS_CMD, args)
    end

    private def to_action(command)
      action = command.to_s
        .gsub(/(?:_+)([a-z])/) { $1.upcase }
        .gsub(/(\A|\s)([a-z])/) { $1 + $2.upcase }

      "org.mpris.MediaPlayer2.Player.#{action}"
    end
  end
end
